package au.com.assignment.marsmission;

import au.com.assignment.marsmission.domain.RoverMoveInstruction;
import au.com.assignment.marsmission.domain.RoverPosition;
import au.com.assignment.marsmission.enums.RoverMove;
import au.com.assignment.marsmission.enums.RoverOrientation;
import org.apache.commons.io.FileUtils;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class MissionControllerTest {

    @Test
    public void initTest() throws IOException {

        File file = File.createTempFile( "instruction", "txt");
        List<String> fileContents = Arrays.asList("5 5", "1 2 N", "LMLMLMLMM", "3 3 E", "MMRMMRMRRM");
        FileUtils.writeLines(file, fileContents);
        MissionController missionController = new MissionController(file.getAbsolutePath());


        Assert.assertEquals(Arrays.asList(new RoverController(new RoverPosition(1,2, RoverOrientation.NORTH)),
                new RoverController(new RoverPosition(3,3, RoverOrientation.EAST))), missionController.getRoverControllers());


        RoverMoveInstruction moveInstruction1 = new RoverMoveInstruction();
        moveInstruction1.setRoverMoves(Arrays.asList(RoverMove.LEFT_TURN, RoverMove.MOVE_FORWARD, RoverMove.LEFT_TURN,
                RoverMove.MOVE_FORWARD, RoverMove.LEFT_TURN, RoverMove.MOVE_FORWARD, RoverMove.LEFT_TURN,
                RoverMove.MOVE_FORWARD, RoverMove.MOVE_FORWARD));

        RoverMoveInstruction moveInstruction2 = new RoverMoveInstruction();
        moveInstruction2.setRoverMoves(Arrays.asList(RoverMove.MOVE_FORWARD, RoverMove.MOVE_FORWARD, RoverMove.RIGHT_TURN,
                RoverMove.MOVE_FORWARD, RoverMove.MOVE_FORWARD, RoverMove.RIGHT_TURN, RoverMove.MOVE_FORWARD, RoverMove.RIGHT_TURN,
                RoverMove.RIGHT_TURN, RoverMove.MOVE_FORWARD));

        Assert.assertEquals(Arrays.asList(moveInstruction1, moveInstruction2), missionController.getRoverInstructions());
    }

    @Test
    public void moveIT() throws  IOException{

        File file = File.createTempFile( "instruction", "txt");
        List<String> fileContents = Arrays.asList("5 5", "1 2 N", "LMLMLMLMM", "3 3 E", "MMRMMRMRRM");
        FileUtils.writeLines(file, fileContents);
        MissionController missionController = new MissionController(file.getAbsolutePath());
        Assert.assertEquals("1 3 N", missionController.move(0));
        Assert.assertEquals("5 1 E", missionController.move(1));
        file.deleteOnExit();
    }
}

