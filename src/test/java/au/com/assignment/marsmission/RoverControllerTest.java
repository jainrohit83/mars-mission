package au.com.assignment.marsmission;

import au.com.assignment.marsmission.domain.RoverPosition;
import au.com.assignment.marsmission.enums.RoverMove;
import au.com.assignment.marsmission.enums.RoverOrientation;
import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class RoverControllerTest {


    @Test
    public void move() throws Exception {

        RoverController roverController = new RoverController(new RoverPosition(2,2, RoverOrientation.NORTH));
        Assert.assertEquals(new RoverPosition(2,3, RoverOrientation.NORTH), roverController.move(RoverMove.MOVE_FORWARD));

        roverController = new RoverController(new RoverPosition(2,2, RoverOrientation.NORTH));
        Assert.assertEquals(new RoverPosition(2,2, RoverOrientation.WEST), roverController.move(RoverMove.LEFT_TURN));

        roverController = new RoverController(new RoverPosition(2,2, RoverOrientation.NORTH));
        Assert.assertEquals(new RoverPosition(2,2, RoverOrientation.EAST), roverController.move(RoverMove.RIGHT_TURN));
    }

    @Test
    public void moveForward() throws Exception {

        RoverController roverController = new RoverController(new RoverPosition(2,2, RoverOrientation.NORTH));
        Assert.assertEquals(new RoverPosition(2,3, RoverOrientation.NORTH), roverController.moveForward(roverController.getRoverPosition()));

        roverController = new RoverController(new RoverPosition(2,2, RoverOrientation.WEST));
        Assert.assertEquals(new RoverPosition(1,2, RoverOrientation.WEST), roverController.moveForward(roverController.getRoverPosition()));

        roverController = new RoverController(new RoverPosition(2,2, RoverOrientation.SOUTH));
        Assert.assertEquals(new RoverPosition(2,1, RoverOrientation.SOUTH), roverController.moveForward(roverController.getRoverPosition()));

        roverController = new RoverController(new RoverPosition(2,2, RoverOrientation.EAST));
        Assert.assertEquals(new RoverPosition(3,2, RoverOrientation.EAST), roverController.moveForward(roverController.getRoverPosition()));
    }

    @Test
    public void turnLeft() throws Exception {

        RoverController roverController = new RoverController(new RoverPosition(2,2, RoverOrientation.NORTH));
        Assert.assertEquals(new RoverPosition(2,2, RoverOrientation.WEST), roverController.turnLeft(roverController.getRoverPosition()));

        roverController = new RoverController(new RoverPosition(2,2, RoverOrientation.WEST));
        Assert.assertEquals(new RoverPosition(2,2, RoverOrientation.SOUTH), roverController.turnLeft(roverController.getRoverPosition()));

        roverController = new RoverController(new RoverPosition(2,2, RoverOrientation.SOUTH));
        Assert.assertEquals(new RoverPosition(2,2, RoverOrientation.EAST), roverController.turnLeft(roverController.getRoverPosition()));

        roverController = new RoverController(new RoverPosition(2,2, RoverOrientation.EAST));
        Assert.assertEquals(new RoverPosition(2,2, RoverOrientation.NORTH), roverController.turnLeft(roverController.getRoverPosition()));
    }

    @Test
    public void turnRight() throws Exception {

        RoverController roverController = new RoverController(new RoverPosition(2,2, RoverOrientation.NORTH));
        Assert.assertEquals(new RoverPosition(2,2, RoverOrientation.EAST), roverController.turnRight(roverController.getRoverPosition()));

        roverController = new RoverController(new RoverPosition(2,2, RoverOrientation.EAST));
        Assert.assertEquals(new RoverPosition(2,2, RoverOrientation.SOUTH), roverController.turnRight(roverController.getRoverPosition()));

        roverController = new RoverController(new RoverPosition(2,2, RoverOrientation.SOUTH));
        Assert.assertEquals(new RoverPosition(2,2, RoverOrientation.WEST), roverController.turnRight(roverController.getRoverPosition()));

        roverController = new RoverController(new RoverPosition(2,2, RoverOrientation.WEST));
        Assert.assertEquals(new RoverPosition(2,2, RoverOrientation.NORTH), roverController.turnRight(roverController.getRoverPosition()));
    }
}