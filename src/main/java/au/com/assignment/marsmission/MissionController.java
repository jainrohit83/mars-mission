package au.com.assignment.marsmission;

import au.com.assignment.marsmission.domain.RoverMoveInstruction;
import au.com.assignment.marsmission.domain.RoverPosition;
import au.com.assignment.marsmission.enums.RoverMove;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Mission controller, keep a reference to all rovers and also provide move instructions.
 */
public class MissionController {

    private List<RoverController> roverControllers= new ArrayList<>();

    private List<RoverMoveInstruction> roverInstructions = new ArrayList<>();

    public MissionController(String file) {
        init(file);
    }

    private void init(String file) {

        List<String> fileContents = MissionHelper.parseFile(file);
        RoverPosition roverPosition1 = MissionHelper.parsePosition(fileContents.get(1));
        roverControllers.add(new RoverController(roverPosition1));
        RoverPosition roverPosition2 = MissionHelper.parsePosition(fileContents.get(3));
        roverControllers.add(new RoverController(roverPosition2));

        roverInstructions.add(MissionHelper.parseMoveInstruction(fileContents.get(2)));
        roverInstructions.add(MissionHelper.parseMoveInstruction(fileContents.get(4)));
    }

    //Test purpose only
    protected List<RoverController> getRoverControllers() {
        return roverControllers;
    }

    //Test purpose only
    protected List<RoverMoveInstruction> getRoverInstructions() {
        return roverInstructions;
    }

    public String move(int rover) {

        RoverController controller = roverControllers.get(rover);
        RoverMoveInstruction instruction = roverInstructions.get(rover);
        for (RoverMove move : instruction.getRoverMoves()) {
            controller.move(move);
        }
        return MissionHelper.toPosition(controller.getRoverPosition());
    }

    public static void main(String[] args) throws IOException {

        if (args.length == 0) {
            throw new IllegalArgumentException("No argument supplied, instruction file path required.");
        }
        MissionController controller = new MissionController(args[0]);
        controller.move(0);
        controller.move(1);
    }

}
