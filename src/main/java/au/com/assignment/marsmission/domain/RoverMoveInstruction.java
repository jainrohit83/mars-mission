package au.com.assignment.marsmission.domain;

import au.com.assignment.marsmission.enums.RoverMove;

import java.util.ArrayList;
import java.util.List;

public class RoverMoveInstruction {

    private List<RoverMove> roverMoves = new ArrayList<>();

    public List<RoverMove> getRoverMoves() {
        return roverMoves;
    }

    public void addRoverMove(RoverMove roverMove) {
        roverMoves.add(roverMove);
    }

    public void setRoverMoves(List<RoverMove> roverMoves) {
        this.roverMoves = roverMoves;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        RoverMoveInstruction that = (RoverMoveInstruction) o;
        return roverMoves != null ? roverMoves.equals(that.roverMoves) : that.roverMoves == null;
    }

    @Override
    public int hashCode() {
        return roverMoves != null ? roverMoves.hashCode() : 0;
    }
}
