package au.com.assignment.marsmission.domain;

import au.com.assignment.marsmission.enums.RoverOrientation;

public class RoverPosition {

    private int x;

    private int y;

    private RoverOrientation direction;

    public RoverPosition(int x, int y, RoverOrientation direction) {

        this.x = x;
        this.y = y;
        this.direction = direction;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public RoverOrientation getDirection() {
        return direction;
    }

    @Override
    public boolean equals(Object o) {

        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        RoverPosition that = (RoverPosition) o;

        if (x != that.x) return false;
        if (y != that.y) return false;
        return direction == that.direction;

    }

    @Override
    public int hashCode() {

        int result = x;
        result = 31 * result + y;
        result = 31 * result + (direction != null ? direction.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "RoverPosition{" +
                "x=" + x +
                ", y=" + y +
                ", direction=" + direction +
                '}';
    }
}
