package au.com.assignment.marsmission.enums;

public enum RoverMove {

    LEFT_TURN("L"),
    RIGHT_TURN("R"),
    MOVE_FORWARD("M");

    private final String code;

    RoverMove(String code) {
        this.code = code;
    }

    public String getCode() {
        return this.code;
    }

    public static RoverMove fromCode(String code) {

        for(RoverMove d: RoverMove.values()) {
            if(d.code.equals(code)) {
                return d;
            }
        }
        return null;
    }
}
