package au.com.assignment.marsmission.enums;

public enum RoverOrientation {

    NORTH("N"),
    EAST("E"),
    SOUTH("S"),
    WEST("W");

    private final String code;

    RoverOrientation(String code) {
        this.code = code;
    }

    @Override
    public String toString() {
        return this.code;
    }

    public String getCode() {
        return this.code;
    }

    public static RoverOrientation fromCode(String code) {

        for(RoverOrientation d: RoverOrientation.values()) {
            if(d.code.equals(code)) {
                return d;
            }
        }
        return null;// not found
    }
}
