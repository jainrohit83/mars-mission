package au.com.assignment.marsmission;


import au.com.assignment.marsmission.domain.RoverPosition;
import au.com.assignment.marsmission.enums.RoverOrientation;
import au.com.assignment.marsmission.enums.RoverMove;
import org.apache.log4j.Logger;


/**
 * Rover controller, keep current location of rover and handle move instructions as provided by mission controller.
 */
public class RoverController {

    private Logger log = Logger.getLogger(RoverController.class);

    private RoverPosition roverPosition;

    public RoverController(RoverPosition roverPosition) {
        this.roverPosition = roverPosition;
    }

    public RoverPosition move(RoverMove move) {

        log.info("Rover move instruction:" + move.name());
        log.debug("Rover position before move:" + roverPosition);
        if (move == RoverMove.MOVE_FORWARD) {
            roverPosition = moveForward(roverPosition);
        } else if (move == RoverMove.LEFT_TURN) {
            roverPosition = turnLeft(roverPosition);
        } else if (move == RoverMove.RIGHT_TURN) {
            roverPosition = turnRight(roverPosition);
        }
        log.info("Rover position after move:" + roverPosition);
        return roverPosition;
    }

    public RoverPosition getRoverPosition() {
        return roverPosition;
    }

    protected RoverPosition moveForward(RoverPosition position) {

        RoverOrientation direction = position.getDirection();
        int x = position.getX();
        int y = position.getY();
        switch (direction) {
            case NORTH:
                y = y + 1;
                break;
            case EAST:
                x = x + 1;
                break;
            case SOUTH:
                y = y - 1;
                break;
            case WEST:
                x = x - 1;
                break;
        }
        return new RoverPosition(x, y, direction);
    }

    protected RoverPosition turnLeft(RoverPosition position) {

        RoverOrientation direction = position.getDirection();
        switch (direction) {
            case NORTH:
                direction = RoverOrientation.WEST;
                break;
            case WEST:
                direction = RoverOrientation.SOUTH;
                break;
            case SOUTH:
                direction = RoverOrientation.EAST;
                break;
            case EAST:
                direction = RoverOrientation.NORTH;
                break;
        }
        return new RoverPosition(position.getX(), position.getY(), direction);
    }

    protected RoverPosition turnRight(RoverPosition position) {

        RoverOrientation direction = position.getDirection();
        switch (direction) {
            case NORTH:
                direction = RoverOrientation.EAST;
                break;
            case EAST:
                direction = RoverOrientation.SOUTH;
                break;
            case SOUTH:
                direction = RoverOrientation.WEST;
                break;
            case WEST:
                direction = RoverOrientation.NORTH;
                break;
        }
        return new RoverPosition(position.getX(), position.getY(), direction);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        RoverController that = (RoverController) o;

        return roverPosition.equals(that.roverPosition);

    }

    @Override
    public int hashCode() {
        return roverPosition.hashCode();
    }
}






