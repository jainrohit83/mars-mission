package au.com.assignment.marsmission;

import au.com.assignment.marsmission.domain.RoverMoveInstruction;
import au.com.assignment.marsmission.domain.RoverPosition;
import au.com.assignment.marsmission.enums.RoverOrientation;
import au.com.assignment.marsmission.enums.RoverMove;
import org.apache.commons.io.FileUtils;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Helper class to provide basic file parsing and  instruction and position parsing provided in instruction file.
 */
public class MissionHelper {

    public static List<String> parseFile(String file) {

        List<String> fileContents = new ArrayList<>();
        try {
            fileContents = FileUtils.readLines(new File(file), "UTF-8");
        } catch (IOException ex) {
            new RuntimeException("Error file reading contents of file:" + file, ex);
        }
        return fileContents;
    }

    public static RoverPosition parsePosition(String roverPositionStr) {

        String[] roverPositionArr = roverPositionStr.split(" ");
        return new RoverPosition(Integer.valueOf(roverPositionArr[0]),
                Integer.valueOf(roverPositionArr[1]), RoverOrientation.fromCode(roverPositionArr[2]));
    }

    public static String toPosition(RoverPosition roverPosition) {
        return roverPosition.getX() + " " + roverPosition.getY() + " " + roverPosition.getDirection().getCode();
    }

    public static RoverMoveInstruction parseMoveInstruction(String roverMoveInstructionStr) {

        RoverMoveInstruction roverMoveInstruction = new RoverMoveInstruction();
        for (String moveInstruction : roverMoveInstructionStr.split("")) {
            roverMoveInstruction.addRoverMove(RoverMove.fromCode(moveInstruction));
        }
        return roverMoveInstruction;
    }
}
